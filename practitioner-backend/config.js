'use strict'

module.exports = {
   port: process.env.PORT || 3002,
   base: 'mongodb://' +process.env.MONGO_URI+ '/practitioner' || 'mongodb://localhost:27017/practitioner',
   SECRET_TOKEN: 'practitioner-nicolas-biscione'
}