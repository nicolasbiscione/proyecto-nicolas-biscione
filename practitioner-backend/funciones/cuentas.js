'use strict'

const Cuenta = require('../modelos/cuenta')

function buscarCuentasCli(req,res){
    let idUsuario = req.params.idUsuario;
    Cuenta.findOne({idUsuario:idUsuario},(err,conCuenta)=>{
        if (err){
            return res.status(500).send({
            respuesta:500,
            mensaje:`Error al consultar cuentas: ${err} ` 
            })
        }    
        if (conCuenta)  {   
            return res.status(200).send({
                respuesta:200,
                cuenta: conCuenta.cuentas, 
                mensaje:'Consulta OK'
            })
        } else {
            return res.status(200).send({
                respuesta:200,
                mensaje:'Cliente sin cuentas'
            })        
        }    
    })               
}

function insertarCuenta(req,res){
    let idUsuario = req.body.idUsuario;
    let numCuenta ;
    Cuenta.find({})
    .sort({ "cuentas.numCuenta": -1 })
    .limit(1)
    .exec((err, numCuentaMax) => {
        if (err) {
            return res.status(500).send({
                respuesta:500,
                mensaje:`Error en el alta de cuenta: ${err} `
            })
        }
        if (numCuentaMax.length != 0) {
            let pos = (numCuentaMax[0].cuentas.length) - 1
            numCuenta = (numCuentaMax[0].cuentas[pos].numCuenta) +1
        } else {
            numCuenta = 1
        }   
    })
    Cuenta.findOne({idUsuario:idUsuario},(err,conCuenta)=>{
        if (conCuenta)  {   
            let cuenta = {
                numCuenta: numCuenta,
                sucuCuenta: req.body.sucuCuenta,
                monedaCuenta: req.body.monedaCuenta,
                tipoCuenta: req.body.tipoCuenta,
                fechaAltaCuenta: new Date(),
                estadoCuenta: true,
                saldoCuenta: 0
                }

            conCuenta.cuentas.push(cuenta);
            conCuenta.save((err, cuenta) => {
                if (err) {
                    return res.status(500).send({
                        respuesta:500,
                        mensaje:`Error en el alta de cuenta: ${err} `
                    })        
                }
                return res.status(200).send({
                    respuesta:200,
                    cuenta: cuenta, 
                    mensaje:'Alta de cuenta realizada correctamente'
                })
            })    
        } else {
            const cuenta = new Cuenta({
                idUsuario: req.body.idUsuario,
                cuentas: {  numCuenta: numCuenta,
                            sucuCuenta: req.body.sucuCuenta,
                            monedaCuenta: req.body.monedaCuenta,
                            tipoCuenta: req.body.tipoCuenta,
                            fechaAltaCuenta:  new Date(),
                            estadoCuenta: true,
                            saldoCuenta: 0 }
            })
            cuenta.save((err, cuenta) => {
                if (err) {
                    return res.status(500).send({
                        respuesta:500,
                        mensaje:`Error en el alta de cuenta: ${err} `, 
                    })
        
                }
                return res.status(200).send({
                    respuesta:200,
                    cuenta: cuenta, 
                    mensaje:'Alta de cuenta realizada correctamente'
                })
            })
        }                
    })    
}

module.exports = {
    insertarCuenta,
    buscarCuentasCli
}