'use strict'

const Disponibilidad = require('../modelos/disponibilidad');
const { subscribe } = require('../routes');

function buscarDispoSuc(req,res){
    let sucursal = req.params.sucursal;
    Disponibilidad.findOne({sucursal:sucursal},(err,data)=>{
        if (err){
            return res.status(500).send({
            respuesta:500,
            mensaje:`Error en la consulta: ${err} ` 
            })
        }    
        if (data)  {   
            return res.status(200).send({
                respuesta:200,
                disponibilidad: data, 
                mensaje:'Consulta OK'
            })
        } else {
            return res.status(200).send({
                respuesta:204,
                mensaje:'Sin información.'
            })        
        }    
    })               
}


function insertarDisponibilidad(req,res){
    let sucursal = req.body.sucursal;
    let cantTuenosHora = req.body.cantTuenosHora;
    Disponibilidad.findOne({sucursal:sucursal},(err,data)=>{
        if (err) {
            return res.status(500).send({
                respuesta:500,
                mensaje:`Error en consulta disponibilidad ${err}`
            })
        } 
        if (data)  {   
            data.cantTuenosHora = cantTuenosHora
            data.save((err, disponibilidad) => {
                if (err) {
                    return res.status(500).send({
                        respuesta:500,
                        mensaje:`Error al actualizar información: ${err} `, 
                    })
        
                }
                return res.status(200).send({
                    respuesta:200,
                    disponibilidad: disponibilidad, 
                    mensaje:'La informacion se actualizo correctamente.'
                })
            })    
        } else {
            const disponibilidad = new Disponibilidad({
                sucursal:  sucursal,
                cantTuenosHora: cantTuenosHora
            })
            disponibilidad.save((err, disponibilidad) => {
                if (err) {
                    return res.status(500).send({
                        respuesta:500,
                        mensaje:`Error al actualizar información: ${err} `, 
                    })
        
                }
                return res.status(200).send({
                    respuesta:200,
                    disponibilidad: disponibilidad, 
                    mensaje:'La informacion se actualizo correctamente.'
                })
            })
        }                
    })    
}

module.exports = {
    buscarDispoSuc,
    insertarDisponibilidad
}