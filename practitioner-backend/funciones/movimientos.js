'use strict'

const Cuenta = require('../modelos/cuenta')


function buscarMovimientos(req,res){
    let numCuenta = req.params.numCuenta;
    Cuenta.findOne({"cuentas.numCuenta":numCuenta},{"cuentas.$":1,_id:0})
    .exec((err, movimientos) => {
        if (err) {
            return res.status(500).send({
                respuesta:500,
                mensaje:`Error en la consulta de movimientos: ${err} `, 
            })
        }
        if (movimientos.cuentas[0].movimientos != 0){
            return res.status(200).send({
                respuesta:200,
                movimientos: movimientos.cuentas[0].movimientos, 
                mensaje:'Consulta de movimiento OK'
            })  
        } else {
            return res.status(200).send({
                respuesta:204,
                mensaje:'Sin movimientos.'
            })
        }
    })
}


function insertarMovimientos(req,res){
    let numCuenta = req.body.numCuenta;
    let numMov;
    Cuenta.findOne({"cuentas.numCuenta":numCuenta},{"cuentas.$":1,_id:0})
    .exec((err, numMovMax) => {
        if (err) {
            return res.status(500).send({
                respuesta:500,
                mensaje:`Error en el alta de cuenta: ${err} `, 
            })
        }
        if (numMovMax.cuentas[0].movimientos.length != 0) {
            let pos = (numMovMax.cuentas[0].movimientos.length) - 1
            let mov = parseInt((numMovMax.cuentas[0].movimientos[pos].numMov).substring((numMovMax.cuentas[0].movimientos[pos].numMov).indexOf('-',0)+1),10) + 1
            numMov = (numMovMax.cuentas[0].movimientos[pos].numMov).substring(0,((numMovMax.cuentas[0].movimientos[pos].numMov).indexOf('-',0))+1) + mov
        } else {
            numMov = numCuenta + '-' + 1
        }   

        if (req.body.impMov > numMovMax.cuentas[0].saldoCuenta && req.body.tipMov ==='Transferencia'){
            return res.status(200).send({
            respuesta:204,
            mensaje:'Saldo Insuficiente'
            })
        } else {
            if (req.body.tipMov ==='Transferencia'){            
                req.body.saldoCueMov = parseInt(numMovMax.cuentas[0].saldoCuenta,10) - parseInt(req.body.impMov,10)
            } else {
                req.body.saldoCueMov = parseInt(numMovMax.cuentas[0].saldoCuenta,10) + parseInt(req.body.impMov,10)
            }    
            Cuenta.findOne({"cuentas.numCuenta":numCuenta},(err,conCuenta)=>{
                if (conCuenta)  {   
                    let movi = {
                        numMov: numMov,
                        impMov: req.body.impMov,
                        tipMov: req.body.tipMov,
                        destinoMov: req.body.destinoMov,
                        saldoCueMov: req.body.saldoCueMov,
                        descMov: req.body.descMov,
                        fechaAltaMov: new Date(),
                        }
                    let posCue;    
                    for (var i = 0; i < conCuenta.cuentas.length; i++){
                        if (conCuenta.cuentas[i].numCuenta == numCuenta) {
                            posCue = i 
                        }    
                    }    
                    
                    conCuenta.cuentas[posCue].movimientos.push(movi);
                    if (req.body.tipMov ==='Transferencia'){
                        conCuenta.cuentas[posCue].saldoCuenta =  parseInt(conCuenta.cuentas[posCue].saldoCuenta,10) - parseInt(req.body.impMov,10)
                    } else{
                        conCuenta.cuentas[posCue].saldoCuenta =  parseInt(conCuenta.cuentas[posCue].saldoCuenta,10) + parseInt(req.body.impMov,10)
                    }    
                    conCuenta.save((err, movimiento) => {
                        if (err) {
                            return res.status(500).send({
                                respuesta:500,
                                mensaje:`Error en el alta de movimiento: ${err} `, 
                            })
                
                        }
                        return res.status(200).send({
                            respuesta:200,
                            cuenta: movimiento, 
                            mensaje:'Alta de movimiento realizada correctamente'
                        })
                    })    
                }             
            })    
        }    
    })    
}

module.exports = {
    insertarMovimientos,
    buscarMovimientos
}