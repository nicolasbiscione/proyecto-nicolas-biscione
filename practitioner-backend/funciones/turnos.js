'use strict'

const Usuario = require('../modelos/usuario')
const Disponibilidad = require('../modelos/disponibilidad')

function buscarTurnosCli(req,res){
    let idUsuario = req.params.idUsuario;
    Usuario.findOne({_id:idUsuario},{"turnos":1,_id:0},(err,data)=>{
        if (err){
            return res.status(500).send({
            respuesta:500,
            mensaje:`Error al consultar turnos: ${err} ` 
            })
        }    
        if (data.turnos)  {   
            return res.status(200).send({
                respuesta:200,
                turnos: data.turnos, 
                mensaje:'Consulta OK'
            })
        } else {
            return res.status(200).send({
                respuesta:200,
                mensaje:'Cliente sin turnos'
            })        
        }    
    })               
}

function buscarTurnosFec(req,res){
    let fechaDesde = req.params.fechaDesde;
    Usuario.find({"turnos.fecha":fechaDesde},{"turnos":1,_id:0,nombre:1,apellido:1,email:1},(err,data)=>{
        if (err){
            return res.status(500).send({
            respuesta:500,
            mensaje:`Error al consultar turnos: ${err} ` 
            })
        }    
        if (data.length>0)  {
            let dataformat =  []
            for (let i=0;i < data.length;i++){
                for (let j=0;j < data[i].turnos.length;j++){
                    if (data[i].turnos[j].fecha ===fechaDesde){
                        dataformat.push({fecha:data[i].turnos[j].fecha, hora: data[i].turnos[j].hora, sucursal: data[i].turnos[j].sucursal, 
                                         nombre:data[i].nombre, apellido:data[i].apellido, email:data[i].email})    
                    }    
                }   
            }
          
            return res.status(200).send({
                respuesta:200,
                turnos: dataformat, 
                mensaje:'Consulta OK'
            })
        } else {
            return res.status(200).send({
                respuesta:200,
                mensaje:'No existen turnos para la fecha seleccionada.'
            })        
        }    
    })               
}

function insertarTurno(req,res){
    let idUsuario = req.body.idUsuario;
    let fecha = req.body.fecha;
    let hora = req.body.hora;
    let sucursal = req.body.sucursal;
    Usuario.findOne({_id:idUsuario,turnos:{$elemMatch:{"fecha":fecha,"hora":hora}}},(err,data)=>{
        if (err) {
            return res.status(500).send({
                respuesta:500,
                mensaje:`Error al registrar turno ${err}`
            })
        } 
        if (data)  {   
            return res.status(200).send({
                respuesta:200,
                mensaje:'Ya existe un turno en este horario.'
                })
        } else {
            Disponibilidad.findOne({sucursal:sucursal},(err,disponibilidad)=>{          
                if (err){
                    return res.status(500).send({
                        respuesta:500,
                        mensaje:`Error en consulta disponibilidad: ${err} ` 
                    })
                }    
                if (disponibilidad)  {
                    Usuario.find({"turnos.fecha":fecha,"turnos.hora":hora},{_id:1},(err,turnosFecha)=>{
                        if (err){
                            return res.status(500).send({
                                respuesta:500,
                                mensaje:`Error en consulta disponibilidad: ${err} ` 
                            })
                        } 
                        if (turnosFecha){
                            if (disponibilidad.cantTuenosHora<=turnosFecha.length){
                                return res.status(200).send({
                                    respuesta:200,
                                    mensaje:`No hay disponibilidad para día/hora seleccionada.` 

                                })
                            } else {
                                    Usuario.findOne({_id:idUsuario},(err,usuario)=>{
                                        if (usuario){
                                                let turno = {
                                                    idUsuario:  idUsuario,
                                                    fecha: fecha,
                                                    hora: hora,
                                                    sucursal: sucursal
                                                    }
                                                usuario.turnos.push(turno);
                                                usuario.save((err, cuenta) => {
                                                    if (err) {
                                                        return res.status(500).send({
                                                            respuesta:500,
                                                            mensaje:`Error en el alta de turno: ${err} `, 
                                                        })
                                            
                                                    }
                                                    return res.status(200).send({
                                                        respuesta:200,
                                                        cuenta: cuenta, 
                                                        mensaje:'Alta de turno realizada correctamente'
                                                    })
                                                })                
                                        }
                                    })

                            }   
                        } else {
                            return res.status(200).send({
                                respuesta:200,
                                cuenta: cuenta, 
                                mensaje:'Por el momento no es posible solicitar un turno para esta sucursal, disculpas. '
                            })                            
                        }

                    })                        
                }
            })

        }
    })    
}

module.exports = {
    buscarTurnosCli,
    insertarTurno,
    buscarTurnosFec
}