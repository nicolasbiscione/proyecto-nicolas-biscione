'use strict'

const { response } = require('express')
const mongoose = require('mongoose')
const jwt = require ('jwt-simple')
const moment = require('moment')
const config = require('../config')
const Usuario = require('../modelos/usuario')
const bcrypt = require('bcrypt')


function ActualizarUsuarioId(req,res){
    let id = req.params.idUsuario;
    Usuario.findByIdAndUpdate(id, {
        $set: {
            nombre: req.body.nombre,
            apellido: req.body.apellido,
            email: req.body.email,
            admin: req.body.admin
        }
    }, { new: true }, (err, usuario) => {
        if (usuario == null) {
            return res.status(200).send({
                respuesta:200,
                mensaje:'No existe usuario' 
                })
        }
        if (err) {
            return res.status(500).send({
                respuesta:500,
                mensaje:`Error al actualizar usuario ${err}`
                })    
        };
        return res.status(200).send({
            respuesta:200,
            mensaje:`La modificación se realizó correctamente.`,
            usuario: usuario
        })
    });
}    

function borrarUsuarioId(req,res){
    let id = req.params.idUsuario;
    Usuario.findByIdAndDelete(id)
    .then(data=>{
        if(data === null) {
            return res.status(200).send({
                respuesta:200,
                mensaje:'No existe usuario' 
                })
        }           
        return res.status(200).send({
                respuesta:200,
                mensaje:'Usuario borrado correctamente.' ,
                usuario: data
        })
    })    
    .catch(err=> {
        return res.status(500).send({
            respuesta:500,
            mensaje:`Error al realizar login ${err}`
            })    
    }) 
}    


function buscarUsuarios(req,res){
    Usuario.find()
    .then(data=>{
        if(data === null) {
            return res.status(200).send({
                respuesta:200,
                mensaje:'No existen usuarios' 
                })
        }           
        return res.status(200).send({
                respuesta:200,
                usuario: data
        })
    })    
    .catch(err=> {
        return res.status(500).send({
            respuesta:500,
            mensaje:`Error al realizar login ${err}`
            })    
    }) 
}    

function buscarUsuarioId(req,res){
    let id = req.params.idUsuario;
    Usuario.findOne({_id:id})
    .then(data=>{
        if(data === null) {
            return res.status(403).send({
                respuesta:403,
                mensaje:'Usuario o password invalido' 
                })
        }           
        return res.status(200).send({
                respuesta:200,
                usuario: data
        })
    })    
    .catch(err=> {
        return res.status(500).send({
            respuesta:500,
            mensaje:`Error al realizar login ${err}`
            })    
    }) 
}    

function login(req,res){
    Usuario.findOne({username:req.body.username})
    .then(data=>{
        if(data === null) {
            return res.status(403).send({
                respuesta:403,
                mensaje:'Usuario o password invalido' 
                })
        } else {
            if(!bcrypt.compareSync(req.body.password, data.password)) { 
                return res.status(403).send({
                    respuesta:403,
                    mensaje:'Usuario o password invalido' 
                    })
            }    
        }    
        return res.status(200).send({
                respuesta:200,
                usuario: data, 
                mensaje:'Usuario logueado correctamente', 
                token: crearToken(data)})
    })
    .catch(err=> {
        return res.status(500).send({
            respuesta:500,
            mensaje:`Error al realizar login ${err}`
            })
    })     
}


function registro(req,res,next){
    Usuario.findOne({username:req.body.username})
        .then(data=> {
            if (data) {
                return res.status(200).send({
                    respuesta:200,
                    mensaje:'El usuario ya existe.'
                    })
            } else{
                var hash = bcrypt.hashSync(req.body.password, 10);
                const usuario = new Usuario({
                    username: req.body.username,
                    password: hash,
                    nombre: req.body.nombre,
                    apellido: req.body.apellido,
                    email: req.body.email,
                    admin:false
                })
                usuario.save((err)=>{
                    if (err) {
                        return res.status(500).send({
                            respuesta:500,
                            mensaje:`Error al registrar usuario ${err}`
                        })
                    }    
                    return res.status(200).send({
                        respuesta:200,
                        mensaje:'Se realizo el alta correctamente'})
                })
            }                
        })
        .catch(err => { 
            if (err) {
                return res.status(500).send({
                    respuesta:500,
                    mensaje:`Error al registrar usuario ${err}`
                })
            }    
        })

}

function crearToken(usuario){
    const payload = {
        sub: usuario._id,
        iat:moment().unix(),
        exp:moment().add(14,'days').unix()
    }
    return jwt.encode(payload,config.SECRET_TOKEN)
}


function verificaToken(req,res){
    if (!req.headers.authorization) {
        res.status(403).send({respuesta: 403, mensaje: 'Usuario no autenticado'})
    }
    const token = req.headers.authorization.split(" ")[1]
    const payload = jwt.decode(token,config.SECRET_TOKEN)

    if (payload.exp <= moment.unix()){
        return res.status(401).send({respuesta: 401,mensaje: 'token invalido'})
    }
    req.user = payload.sub
    return res.status(200).send({respuesta: 200,user:req.user ,mensaje: 'login correcto'})
}

function verificaAutentificacion(req,res,next){
    if (!req.headers.authorization) {
        res.status(403).send({respuesta: 403, mensaje: 'Usuario no autenticado'})
    }
    const token = req.headers.authorization.split(" ")[1]
    const payload = jwt.decode(token,config.SECRET_TOKEN)

    if (payload.exp <= moment.unix()){
        return res.status(401).send({respuesta: 401,mensaje: 'token invalido'})
    }
    req.user = payload.sub
    next()
}

module.exports = {
    login,
    registro,
    verificaToken,
    buscarUsuarioId,
    buscarUsuarios,
    borrarUsuarioId,
    ActualizarUsuarioId,
    verificaAutentificacion
}