'use strict'

const express = require('express');
const bodyParser = require('body-parser')
const mongoose = require('mongoose');
const config = require('./config');
//const Personas = require('./modelos/personas');
const api = require('./routes/index')
var cors = require('cors');
//const passport = require('passport');
//const passportLocal = require('passport-local').Strategy;


const app = express();

app.use(cors())
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json())
app.use('/api', api)


//app.post("/login" ,
//passport.authenticate('local',{
//    successRedirect: "/",
//    failureRedirect: "login"
//}));


mongoose.connect(config.base,{ useNewUrlParser: true ,useUnifiedTopology: true},(err,res)=>{
    if(err) {
        console.log(`Error al inciar base de datos: ${err}`)
    } else {
        console.log(`Base de datos ok`)
    }    
    app.listen(config.port,()=> console.log(`Servidor en puerto ${config.port} `));   
})

