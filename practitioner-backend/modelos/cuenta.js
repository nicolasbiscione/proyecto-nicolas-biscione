'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const CuentaSchema = Schema ({
    idUsuario:  {type: mongoose.Types.ObjectId, required: true },
    cuentas:[{ numCuenta: {type: Number, required: false },
              sucuCuenta: {type:String },
              monedaCuenta: {type:String },
              tipoCuenta: {type:String },
              fechaAltaCuenta: {type:Date},
              estadoCuenta: {type:Boolean},
              saldoCuenta: {type:Number},
              movimientos: [{numMov: {type: String, required: false },
                            impMov: {type: Number, required: false },
                            tipMov: {type: String, required: false },
                            destinoMov: {type: String },
                            saldoCueMov: {type: Number, required: false },
                            descMov: String,
                            fechaAltaMov: {type:Date}
                           }]
            } ]

}, { collection: 'cuenta' });
module.exports= mongoose.model('Cuenta',CuentaSchema)
