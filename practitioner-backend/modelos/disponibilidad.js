'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Disponibilidad = Schema ({
    cantTuenosHora: {type: Number, required: true },
    sucursal: {type: String, required: true }
}, { collection: 'disponibilidad' });
module.exports= mongoose.model('Disponibilidad',Disponibilidad)
