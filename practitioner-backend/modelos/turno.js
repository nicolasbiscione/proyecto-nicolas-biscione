'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const TurnoSchema = Schema ({
    idUsuario:  {type: mongoose.Types.ObjectId, required: true },
    fecha: {type: Date, required: true },
    hora: {type: String, required: true },
    sucursal: {type: String, required: false }
}, { collection: 'turno' });
module.exports= mongoose.model('Turno',TurnoSchema)
