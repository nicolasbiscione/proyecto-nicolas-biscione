'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UsuariosSchema = new Schema ({
    username: {type:String, unique:true, lowercase:true, require:true},
    password: {type:String},
    nombre: String,
    apellido: String,
    email: String,
    fechaAlta: {type:Date, default: Date.now()},
    admin: Boolean,
    turnos: [{fecha: {type: String, required: false },
              hora: {type: String, required: false },
              sucursal: {type: String, required: false }
            }]           
})

module.exports= mongoose.model('Usuario',UsuariosSchema)
