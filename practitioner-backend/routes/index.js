'use strict'

const express = require('express');
const funcionesUsuario = require('../funciones/usuarios')
const funcionesMovimientos = require('../funciones/movimientos')
const funcionesCuentas = require('../funciones/cuentas')
const funcionesTurnos = require('../funciones/turnos')
const funcionesDisponibilidad = require('../funciones/disponibilidad')
const api = express.Router();

//Verifica login
api.post("/login" ,funcionesUsuario.login)

//Verifica token
api.post("/verifToken" ,funcionesUsuario.verificaToken)

//Realizar registro
api.post("/registro" , funcionesUsuario.registro)

//Busca datos Usuarios 
api.get("/usuarios/" , funcionesUsuario.buscarUsuarios)

//Busca datos Usuario por ID 
api.get("/usuarios/:idUsuario" , funcionesUsuario.buscarUsuarioId)

//Borra datos Usuarios por ID 
api.delete("/usuarios/:idUsuario" , funcionesUsuario.borrarUsuarioId)

//Actualiza datos Usuarios por ID 
api.put("/usuarios/:idUsuario" , funcionesUsuario.ActualizarUsuarioId)

//Realizar alta cuenta
api.post("/cuentas" , funcionesCuentas.insertarCuenta)

//Realizar consulta de cuentas del usuario
api.get("/cuentas/:idUsuario" , funcionesCuentas.buscarCuentasCli)

//Realizar alta movimiento
api.post("/movimientos" , funcionesMovimientos.insertarMovimientos)

//Realizar consultas de movimientos de cuenta
api.get("/movimientos/:numCuenta" , funcionesMovimientos.buscarMovimientos)

//Realizar consulta de turnos del cliente
api.get("/turnos/:idUsuario" , funcionesTurnos.buscarTurnosCli)

//Realizar consulta de turnos por fecha
api.get("/turnosFecha/:fechaDesde" , funcionesTurnos.buscarTurnosFec)

//Realizar alta de turno
api.post("/turnos" , funcionesTurnos.insertarTurno)

//Realizar alta de parametria disponibilidad
api.post("/disponibilidad" , funcionesDisponibilidad.insertarDisponibilidad)

//Realizar consulta  de parametria disponibilidad por sucursal
api.get("/disponibilidad/:sucursal" , funcionesDisponibilidad.buscarDispoSuc)

module.exports = api