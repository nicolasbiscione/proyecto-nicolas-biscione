import { LitElement, html, css } from 'lit-element';
import './login.js';
import './main.js';
import './registro.js';
import '@material/mwc-snackbar';
import '@material/mwc-button';
import '@material/mwc-textfield';
import '@material/mwc-icon-button';


export class ProyectoPractitioner extends LitElement {
  static get properties() {
    return {
      flow: String,
      login: Boolean,
      mensaje: String,
      userId: String,
      userName: String,
      admin:Boolean
    };
  }

  static get styles() {
    return css`

    `;
  }

  constructor(){
    super();
    this.userId = '';
    this.userName='';
    this.login = false;
    this.mensaje = '';
    this.admin = ';'
    this.flow = 'login'
  }

  render() {
    return html`
        <mwc-snackbar
        labelText="${this.mensaje}"
        timeoutMs="4000">
        <mwc-icon-button icon="close" slot="dismiss"></mwc-icon-button>
        </mwc-snackbar>
        ${(this.login)?
          html`<practitioner-main userId=${this.userId}  userName=${this.userName} @mensajes=${this.informarMensajes} @logout=${this.verificarLogin}></practitioner-main>` :
          (this.flow==='login')?
          html`<practitioner-login @login=${this.verificarLogin}></practitioner-login>` :
          html`<practitioner-registro @registro=${this.verificarRegistro}></practitioner-registro>` 
        } 
    `;
  }


  informarMensajes(e) {
    let msj = e.detail.mensaje
    this.mostrarMensaje(msj)
  }

  mostrarMensaje(msj){
    if (msj) {
      this.mensaje = msj;
      let abrirMsj = this.shadowRoot.querySelector('mwc-snackbar');
      abrirMsj.show();
    }
  }

  verificarRegistro(e){
    this.flow = e.detail.flow
    this.mostrarMensaje(e.detail.mensaje)
  }

  verificarLogin(e){
    if(e.detail.respuesta===200){
      this.userId = e.detail.usuario._id
      this.userName = e.detail.usuario.username
      this.admin = false
      this.flow = e.detail.flow
      let token = localStorage.getItem('token-practitioner')
      if (!token){
        this.login = false
      } else {
        this.login = true
      }
    } else {
      this.flow = e.detail.flow
      this.login = false
    }
    this.mostrarMensaje(e.detail.mensaje)
  }
}



