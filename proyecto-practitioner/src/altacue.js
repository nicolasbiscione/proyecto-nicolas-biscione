import { LitElement, html, css } from 'lit-element';
import "@material/mwc-icon-button";
import "@material/mwc-icon";
import "@material/mwc-button";
import "@material/mwc-textfield";
import '@material/mwc-select';
import '@material/mwc-list/mwc-list-item';


export class AltaCue extends LitElement {
  static get properties() {
    return {
      userId: String,
      sucuCuenta: String,
      monedaCuenta: String,
      tipoCuenta: String
    };
  }

  static get styles() {
    return css`
    .altacue-form {
      display: grid;
      grid-template-columns: 1fr;
      padding: 1em;

    }

    mwc-select,
    mwc-textfield {
      width: 40%;
      margin: auto;
      padding: .8em;
      --mdc-theme-primary: white;
      --mdc-shape-small:28px;
      --mdc-text-field-outlined-idle-border-color: rgba(0, 0, 0, 1.1);
      --mdc-text-field-outlined-hover-border-color: rgba(255, 255, 255, 2);
      --mdc-select-outlined-idle-border-color: rgba(0, 0, 0, 1.1);
      --mdc-select-outlined-hover-border-color: rgba(255, 255, 255, 2);
    }

    mwc-button {
      --mdc-theme-primary: #454545;
    }
    `;
  }

  constructor(){
    super();
  }

  render() {
    return html`
          <br/>
          <h1>Alta Cuenta</h1>
          <div class="altacue-form">
            <mwc-select id="sucuCuenta" required validationMessage="Es obligatorio" label="Sucursal"
              outlined @click="${ this.validarDatos }">         
              <mwc-list-item value="Caseros">Caseros</mwc-list-item>
              <mwc-list-item value="Caballito">Caballito</mwc-list-item>
              <mwc-list-item value="Olivos">Olivos</mwc-list-item>
            </mwc-select>
            <mwc-select id="monedaCuenta" required validationMessage="Es obligatorio" label="Moneda"
              outlined @click="${ this.validarDatos }">            
              <mwc-list-item value="ARS">ARS</mwc-list-item>
              <mwc-list-item value="USD">USD</mwc-list-item>
              <mwc-list-item value="EUR">EUR</mwc-list-item>
            </mwc-select>
            <mwc-select id="tipoCuenta" class="block" required validationMessage="Es obligatorio" label="Tipo de Cuenta"
              outlined @click="${ this.validarDatos }">            
              <mwc-list-item value="CA">CA</mwc-list-item>
              <mwc-list-item value="CC">CC</mwc-list-item>
            </mwc-select>
          </div>  
          <div class="opciones">
            <mwc-button raised="" id="alta" label="Alta" icon="undo" trailingicon="" @click="${this.altaCuenta}" disabled></mwc-button>
          </div>        
    `;
  }



  informarEvento(evento,flow,data) {
    let mensaje = (data.mensaje)?data.mensaje:''
    let event = new CustomEvent(evento, {
        bubbles: true,
        composed: true,
        detail: {flow:flow,
                mensaje:mensaje}
    });
    this.dispatchEvent(event);
  }

  validarDatos(){
    if (this.getField('sucuCuenta').checkValidity() &&
        this.getField('monedaCuenta').checkValidity() &&
        this.getField('tipoCuenta').checkValidity()) {
          this.getField('alta').disabled = false
    } else {
          this.getField('alta').disabled = true
    }
  }
  
async  altaCuenta(){
    this.sucuCuenta= this.getField('sucuCuenta').value;
    this.monedaCuenta= this.getField('monedaCuenta').value;
    this.tipoCuenta= this.getField('tipoCuenta').value;
    let data = {idUsuario:this.userId, sucuCuenta: this.sucuCuenta ,monedaCuenta:this.monedaCuenta,tipoCuenta:this.tipoCuenta};
    await fetch("http://localhost:3002/api/cuentas", {
          method: "POST",
          mode: "cors",
          headers:{
          "Content-Type": "application/json",
          "Accept": "application/json"
            },
            body: JSON.stringify(data)
          })
          .then(response => response.json())
          .then(data => {
              if (data.respuesta === 200) {
                this.informarEvento('mensajes','cuentas',data);
              }  else {
                this.informarEvento('mensajes','altacuentas',data);
              }
          })
  }

  getField( fieldName){
    return this.shadowRoot.getElementById(fieldName);
  }      
  
}

customElements.define('practitioner-altacue', AltaCue);
