import { LitElement, html, css } from 'lit-element';
import "@material/mwc-icon-button";
import "@material/mwc-icon";
import "@material/mwc-button";
import "@material/mwc-textfield";
import '@material/mwc-select';
import '@material/mwc-list/mwc-list-item';


export class AltaMov extends LitElement {
  static get properties() {
    return {
      numCuenta: String,
      impMov: Number,
      tipMov: String,
      destinoMov: String,
      saldoCueMov: Number,
      descMov: String
    };
  }

  static get styles() {
    return css`
      .altamov-form {
        display: grid;
        grid-template-columns: 1fr 1fr;
        padding: 1em;

      }

      mwc-select,
      mwc-textfield {
        width: 90%;
        padding: .8em;
        --mdc-theme-primary: white;
        --mdc-shape-small:28px;
        --mdc-text-field-outlined-idle-border-color: rgba(0, 0, 0, 1.1);
        --mdc-text-field-outlined-hover-border-color: rgba(255, 255, 255, 2);
        --mdc-select-outlined-idle-border-color: rgba(0, 0, 0, 1.1);
        --mdc-select-outlined-hover-border-color: rgba(255, 255, 255, 2);
      }

      mwc-button {
        --mdc-theme-primary: #454545;
      }
      `;
  }

  constructor(){
    super();
  }

  render() {
    return html`
        <div class="contenido">
          <br>
          <h1>Alta Movimiento</h1>
          <div class="altamov-form">              
            <mwc-textfield id="impMov" required validationMessage="Campo obligatorio" label="Importe" 
              type="number" outlined @input="${ this.validarDatos }">
            </mwc-textfield>
            <mwc-select id="tipMov" required validationMessage="Es obligatorio" label="Tipo Movimiento"
              outlined @click="${ this.validarDatos }">            
              <mwc-list-item></mwc-list-item>
              <mwc-list-item value="Deposito">Deposito</mwc-list-item>
              <mwc-list-item value="Transferencia">Transferencia</mwc-list-item>
            </mwc-select>
            <mwc-textfield id="destinoMov" required validationMessage="Es obligatorio" label="Destino" 
                 outlined @input="${ this.validarDatos }">
            </mwc-textfield>
            <mwc-textfield id="descMov" required validationMessage="Es obligatorio" label="Descripción"  
                outlined @input="${ this.validarDatos }" >
            </mwc-textfield>
          </div>
          <mwc-button raised="" id="alta" label="Alta" icon="undo" trailingicon="" @click="${this.altaMovimiento}" disabled></mwc-button>
        </div>       
    `;
  }

  informarEvento(evento,flow,data) {
    let mensaje = (data.mensaje)?data.mensaje:''
    let event = new CustomEvent(evento, {
        bubbles: true,
        composed: true,
        detail: {flow:flow,
                mensaje:mensaje}
    });
    this.dispatchEvent(event);
  }


  validarDatos(){
    if (this.getField('tipMov').value === 'Transferencia'){
      this.getField('destinoMov').disabled = false
    } else {
      this.getField('destinoMov').disabled = true
      this.getField('destinoMov').value = '-'
    }
    if (this.getField('impMov').checkValidity() &&
        this.getField('tipMov').checkValidity() &&
        this.getField('destinoMov').checkValidity() &&
        this.getField('descMov').checkValidity()) {
          this.getField('alta').disabled = false
    } else {
          this.getField('alta').disabled = true
    }
  }
  
async  altaMovimiento(){

    this.impMov= this.getField('impMov').value;
    this.tipMov= this.getField('tipMov').value;
    this.destinoMov= this.getField('destinoMov').value;
    this.descMov= this.getField('descMov').value;
    let data = {numCuenta:this.numCuenta, impMov: this.impMov ,tipMov:this.tipMov,destinoMov:this.destinoMov,descMov:this.descMov};
    await fetch("http://localhost:3002/api/movimientos", {
          method: "POST",
          mode: "cors",
          headers:{
          "Content-Type": "application/json",
          "Accept": "application/json"
            },
            body: JSON.stringify(data)
          })
          .then(response => response.json())
          .then(data => {
              if (data.respuesta === 200) {
                this.informarEvento('mensajes','cuentas',data);
              }  else {
                this.informarEvento('mensajes','altamovimientos',data);
              }
          })
  }
 

  getField( fieldName){
    return this.shadowRoot.getElementById(fieldName);
  }      
  
}

customElements.define('practitioner-altamov', AltaMov);
