import { LitElement, html, css } from 'lit-element';
import "@material/mwc-icon-button";
import "@material/mwc-icon";
import "@material/mwc-button";
import "@material/mwc-textfield";
import '@material/mwc-select';
import '@material/mwc-list/mwc-list-item';
import '@vaadin/vaadin-date-picker/vaadin-date-picker';
import '@vaadin/vaadin-time-picker/vaadin-time-picker';


export class AltaTurno extends LitElement {
  static get properties() {
    return {
      userId: String,
      fecha: Date,
      fechaDia: Date,
      hora: Date,
      sucursal: String
    };
  }

  static get styles() {
    return css`
      .contenido {
        width: 0%;      
        height: 100%;
        background: #005CE5;
        box-shadow: 0 0 15px #ddd;
        top: 50%;
        left: 50%;
        color: #fff;
        box-sizing: border-box;
        padding: 30px 30px;
        display: block;
        text-align: center;
        border-radius:10px;
        opacity: 0;
        animation-name: carga;
        animation-duration: 1s; 
        animation-iteration-count: 1;
        animation-fill-mode: forwards;
      }
      
      @keyframes carga{
        from {opacity: 0;}
        to {opacity: 1;}
        from {width: 1%;}
        to {width: 100%;}
      }

      .altaturno-form {
        display: grid;
        grid-template-columns: 1fr 1fr;
        margin-left: 20%;
        margin-right: 20%;
        left: 50%;
        padding: 1em;
      }

      .altaturno-form .block {
        grid-column: 1/3;
      }  

      mwc-select,
      mwc-textfield {
        width: 60%;
        padding: .8em;
        margin: auto;
        --mdc-theme-primary: white;
        --mdc-shape-small:28px;
        --mdc-text-field-outlined-idle-border-color: rgba(0, 0, 0, 1.1);
        --mdc-text-field-outlined-hover-border-color: rgba(255, 255, 255, 2);
        --mdc-select-outlined-idle-border-color: rgba(0, 0, 0, 1.1);
        --mdc-select-outlined-hover-border-color: rgba(255, 255, 255, 2);
  
      }

      vaadin-time-picker,
      vaadin-date-picker {
        width: 90%;
        padding: .8em;
        margin: auto;
        color: #fff;
        --lumo-header-text-color: ##000000;;
        --lumo-body-text-color: #ffffff;
        --lumo-secondary-text-color: #000000;
        --lumo-tertiary-text-color:#ffffff;
        --lumo-disabled-text-color:#000000;
        --lumo-primary-color-10pct: #000000;
        --lumo-primary-color-50pct: #000000;
        --lumo-primary-text-color: #ffffff;
        -webkit-backdrop-filter: blur(60px);
      }

      mwc-button {
        --mdc-theme-primary: #454545;
      }
    `;
  }

  constructor(){
    super();
    var hoy = new Date();
    var dd = hoy.getDate()+1;
    var mm = hoy.getMonth()+1;
    var yyyy = hoy.getFullYear();
    this.fechaDia = yyyy+'-'+mm+'-'+dd
  }

  render() {
    return html`
          <br>
          <div class="contenido">
            <h1>Solicitar Turno</h1>
            <div class="altaturno-form">
              <vaadin-date-picker  id="fecha" min=${this.fechaDia} label="Seleccione la fecha">
              </vaadin-date-picker>
              <vaadin-time-picker  id="hora" min="09:00" max="14:00" preventInvalidInput label="Seleccione la hora">
              </vaadin-time-picker>
              <mwc-select id="sucursal" class="block"
                required  label="Sucursal"
                outlined @click="${ this.validarDatos }">         
                <mwc-list-item value="Caseros">Caseros</mwc-list-item>
                <mwc-list-item value="Caballito">Caballito</mwc-list-item>
                <mwc-list-item value="Olivos">Olivos</mwc-list-item>
              </mwc-select>
            </div>
            <mwc-button raised="" id="alta" label="Confirmar" icon="undo" trailingicon="" @click="${this.altaTurno}" disabled></mwc-button>
            <p>
            <vaadin-grid >
              <vaadin-grid-column></vaadin-grid-column>
              <vaadin-grid-column></vaadin-grid-column>
              <vaadin-grid-column></vaadin-grid-column>
            </vaadin-grid>          
          </div>  
    `;
  }

  firstUpdated(){
      this.verTurnos()
      var datepicker = this.shadowRoot.querySelector('vaadin-date-picker#fecha');
      datepicker.set('i18n.weekdaysShort', ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab']);
      datepicker.set('i18n.monthNames', ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
                                        'Junio', 'Julio', 'Agosto', 'Septiembre',
                                        'Octubre', 'Noviembre', 'Diciembre']);
      datepicker.set('i18n.formatDate', function(dateObject) {
         return [dateObject.day,dateObject.month+1, dateObject.year].join('/');
      });
      datepicker.set('i18n.parseDate', function(text) {
        var parts = text.split('/');
        if (parts.length === 3) {
          return {
            day: parts[0],
            month: parts[1]-1,
            year: parts[2]
          };
        }  
      });            
  }

  informarEvento(evento,flow,data) {
    let mensaje = (data.mensaje)?data.mensaje:''
    let event = new CustomEvent(evento, {
        bubbles: true,
        composed: true,
        detail: {flow:flow,
                mensaje:mensaje}
    });
    this.dispatchEvent(event);
  }


  validarDatos(){
    if (this.getField('fecha').checkValidity() &&
        this.getField('hora').checkValidity() &&
        this.getField('sucursal').checkValidity()) {
          this.getField('alta').disabled = false
    } else {
          this.getField('alta').disabled = true
    }
  }
  
async  altaTurno(){

    this.fecha= this.getField('fecha').value;
    this.hora = this.getField('hora').value;
    this.sucursal = this.getField('sucursal').value;
    let data = {idUsuario:this.userId, fecha: this.fecha ,hora:this.hora,sucursal:this.sucursal};
    await fetch("http://localhost:3002/api/turnos", {
          method: "POST",
          mode: "cors",
          headers:{
          "Content-Type": "application/json",
          "Accept": "application/json"
            },
            body: JSON.stringify(data)
          })
          .then(response => response.json())
          .then(data => {
              if (data.respuesta === 200) {
                this.verTurnos()
                this.informarEvento('mensajes','altamovimientos',data);
              }
          })
  }
 
  async verTurnos(){

    const grid = this.shadowRoot.querySelector('vaadin-grid');          
    await fetch(`http://localhost:3002/api/turnos/${this.userId}`, {
      method: "GET"})
      .then(response => response.json())
      .then(data => {
          if (data.respuesta === 200) {
            grid.items = data.turnos;
          }
      })
    
    grid.heightByRows = true;
    grid.rowsDraggable = true;
    const columns = grid.querySelectorAll('vaadin-grid-column');    
    columns[0].autoWidth = true;
    columns[1].autoWidth = true;
    columns[2].autoWidth = true;      
    columns[0].headerRenderer = function(root) {root.textContent = 'Fecha';}
    columns[1].headerRenderer = function(root) {root.textContent = 'Horario';};
    columns[2].headerRenderer = function(root) {root.textContent = 'Sucursal';};
    columns[0].renderer = function(root, column, rowData) {root.textContent = rowData.item.fecha;} 
    columns[1].renderer = function(root, column, rowData) {root.textContent = rowData.item.hora;} 
    columns[2].renderer = function(root, column, rowData) {root.textContent = rowData.item.sucursal;} 
    grid.recalculateColumnWidths()
  }
 


  getField( fieldName){
    return this.shadowRoot.getElementById(fieldName);
  }      
  
}

customElements.define('practitioner-altaturno', AltaTurno);
