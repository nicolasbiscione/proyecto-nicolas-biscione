import { LitElement, html, css } from 'lit-element';
import "@material/mwc-icon-button";
import "@material/mwc-icon";
import "@material/mwc-button";
import "@material/mwc-textfield";
import "@material/mwc-textfield";
import '@material/mwc-snackbar';
import '@vaadin/vaadin-grid/vaadin-grid.js';
import '@vaadin/vaadin-checkbox/vaadin-checkbox';
import './usuarios'
import './grilla'
import './altamov'
import './altacue'


export class Cuentas extends LitElement {
  static get properties() {
    return {
      userId: { type: String},
      flow: { type: String},
      data: { type: String},
      numCuenta: { type: String},
      mensaje: { type: String}
    };
  }

  static get styles() {
    return css`
   
    .contenido {
      width: 0%;      
      height: 100%;
      background: #005CE5;
      box-shadow: 0 0 15px #ddd;
      top: 50%;
      left: 50%;
      color: #fff;
      box-sizing: border-box;
      padding: 30px 30px;
      display: block;
      text-align: center;
      border-radius:10px;
      opacity: 0;
      animation-name: carga;
      animation-duration: 1s; 
      animation-iteration-count: 1;
      animation-fill-mode: forwards;
    }
    
    @keyframes carga{
      from {opacity: 0;}
      to {opacity: 1;}
      from {width: 1%;}
      to {width: 100%;}
    }

    mwc-button {
      --mdc-theme-primary: #454545;
    }

    `;
  }

  constructor(){
    super();
    this.flow='cuentas';
    this.data='';
  }

  render() {
    return html`
    <mwc-snackbar
    labelText="${this.mensaje}"
    timeoutMs="4000">
    <mwc-icon-button icon="close" slot="dismiss"></mwc-icon-button>
    </mwc-snackbar>
    <div class="contenido">
    <input id="serteoFlowMov" type="hidden" @click=${() => this.seteoMenu('movimientos')}>
    <input id="serteoFlowAltCue" type="hidden" @click=${() => this.seteoMenu('altacuentas')}>
    <input id="serteoFlowAltMov" type="hidden" @click=${() => this.seteoMenu('altamovimientos')}>
    ${(this.flow ==='cuentas' || this.flow ==='movimientos')?
      html `
      <practitioner-grilla userId=${this.userId} @flow=${this.flowGrilla}></practitioner-grilla>
      <br><mwc-button raised="" label="Alta Cuenta" icon="undo" trailingicon="" @click="${() =>this.seteoMenu('altacuentas')}"></mwc-button>`:
      (this.flow ==='altacuentas')?
      html `<practitioner-altacue userId=${this.userId} @mensajes=${this.informarMensajes}></practitioner-altacue>`:
        (this.flow ==='altamovimientos')?
        html `<practitioner-altamov numCuenta=${this.numCuenta} @mensajes=${this.informarMensajes}></practitioner-altamov>`:``
    }    
    ${this.flow!='cuentas'?
      html `<br><mwc-button raised="" label="Volver" icon="undo" trailingicon="" @click="${() =>this.seteoMenu('cuentas')}"></mwc-button>`:  ``
    }
    </div>
    `
  }

  informarMensajes(e) {
    this.seteoMenu(e.detail.flow)
    let msj = e.detail.mensaje
    if (msj) {
      this.mensaje = msj;
      let abrirMsj = this.shadowRoot.querySelector('mwc-snackbar');
      abrirMsj.show();
    }
  }

  flowGrilla(e) {
    this.seteoMenu(e.detail.accion)
    this.numCuenta = e.detail.numCuenta
  }

  seteoMenu(opcion) {
    this.flow = opcion;
    const grid = this.shadowRoot.querySelector('vaadin-grid');
  }


  async armarGrilla(){
    const flow = this.flow
    const serteoFlowMov = this.shadowRoot.getElementById('serteoFlowMov');
    const serteoFlowAltMov = this.shadowRoot.getElementById('serteoFlowAltMov');
    const grid = this.shadowRoot.querySelector('vaadin-grid');
    let url;

    if (flow==='cuentas') {
        url = `http://localhost:3002/api/cuentas/${this.userId}`
    } else {
        url = `http://localhost:3002/api/movimientos/${grid.selectItem}`
    }

  await fetch(url, {method: "GET"}) 
    .then(response => response.json())
    .then(data => {
      if (data.respuesta === 200) {
        this.data = data;
      }  
    })
    grid.heightByRows = true;
    grid.rowsDraggable = true;

    const columns = grid.querySelectorAll('vaadin-grid-column');
    for (let i = 0; i < columns.length; i++) {
      columns[i].autoWidth = true;
      columns[i].renderer = function(root) {root.textContent = ''}
    }

    if (flow==='cuentas') {
      grid.items = this.data.cuenta;
      columns[0].headerRenderer = function(root) {root.textContent = 'Numero Cuenta';}
      columns[1].headerRenderer = function(root) {root.textContent = 'CC/CA';};
      columns[2].headerRenderer = function(root) {root.textContent = 'Sucursal';};
      columns[3].headerRenderer = function(root) {root.textContent = 'Moneda';};
      columns[4].headerRenderer = function(root) {root.textContent = 'Saldo';};
      columns[5].headerRenderer = function(root) {root.textContent = 'Movimientos';};
      columns[0].renderer = function(root, column, rowData) {root.textContent = rowData.item.numCuenta;} 
      columns[1].renderer = function(root, column, rowData) {root.textContent = rowData.item.tipoCuenta;} 
      columns[2].renderer = function(root, column, rowData) {root.textContent = rowData.item.sucuCuenta;} 
      columns[3].renderer = function(root, column, rowData) {root.textContent = rowData.item.monedaCuenta;} 
      columns[4].renderer = function(root, column, rowData) {root.textContent = rowData.item.saldoCuenta;} 
      columns[5].renderer = function(root, column, rowData) {
        root.innerHTML = '';
        const alertBtn = window.document.createElement('button');
        alertBtn.textContent = 'Movimientos';
        alertBtn.addEventListener('click', function(event) {
          grid.selectItem=rowData.item.numCuenta
          grid.render();
          serteoFlowMov.click()
        });
        const textNode = window.document.createTextNode(' ');
  
        root.appendChild(alertBtn);
        root.appendChild(textNode);
      };
      columns[6].renderer = function(root, column, rowData) {
        root.innerHTML = '';
        const alertBtn = window.document.createElement('button');
        alertBtn.textContent = 'Operar';
        alertBtn.addEventListener('click', function(event) {
          grid.selectItem=rowData.item.numCuenta
          grid.render();
          serteoFlowAltMov.click()
        });
        const textNode = window.document.createTextNode(' ');
  
        root.appendChild(alertBtn);
        root.appendChild(textNode);
      };      
    } else {
      if (flow==='movimientos') {
        grid.items = this.data.movimientos
        columns[0].headerRenderer = function(root) {root.textContent = 'Numero Movimiento';}
        columns[1].headerRenderer = function(root) {root.textContent = 'Tipo Movimiento';}
        columns[2].headerRenderer = function(root) {root.textContent = 'Importe Movimiento';}
        columns[3].headerRenderer = function(root) {root.textContent = 'Destino Movimiento';}
        columns[4].headerRenderer = function(root) {root.textContent = 'Descripción';}
        columns[5].headerRenderer = function(root) {root.textContent = 'Saldo Cuenta';}
        columns[0].renderer = function(root, column, rowData) {root.textContent = (rowData.item.numMov)?rowData.item.numMov:'';} 
        columns[1].renderer = function(root, column, rowData) {root.textContent = (rowData.item.tipMov)?rowData.item.tipMov:'';} 
        columns[2].renderer = function(root, column, rowData) {root.textContent = (rowData.item.impMov)?rowData.item.impMov:'';} 
        columns[3].renderer = function(root, column, rowData) {root.textContent = (rowData.item.destinoMov)?rowData.item.destinoMov:'';} 
        columns[4].renderer = function(root, column, rowData) {root.textContent = (rowData.item.descMov)?rowData.item.destinoMov:'';} 
        columns[5].renderer = function(root, column, rowData) {root.textContent = (rowData.item.saldoCueMov)?rowData.item.saldoCueMov:'';} 
        columns[6].renderer = function(root, column, rowData) {root.textContent = '';} 

      }
    }      
    grid.recalculateColumnWidths()
  }


  logout(){
    localStorage.removeItem('token-practitioner')
    setTimeout(() => {location.reload()}, 500);
  }      
}

customElements.define('practitioner-cuentas', Cuentas);

