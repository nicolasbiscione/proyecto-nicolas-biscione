let informarEvento = (evento,flow,data) => {
    let mensaje = (data.mensaje)?data.mensaje:''
    let event = new CustomEvent(evento, {
        detail: {flow:flow,
                mensaje:mensaje}
    });
    dispatchEvent(event);
  }

let getField = (fieldName) => {
    return this.shadowRoot.getElementById(fieldName);
  } 

let clearInputs = (arraysId) => {
    for (let i = 0; i < arraysId.length; i++){
        this.getField(arraysId[i]).value = "";
    }
  }  

export {
   informarEvento,
   getField,
   clearInputs
};  