import { LitElement, html, css } from 'lit-element';
import "@material/mwc-icon-button";
import "@material/mwc-icon";
import "@material/mwc-button";
import "@material/mwc-textfield";
import "@material/mwc-textfield";
import '@vaadin/vaadin-grid/vaadin-grid.js';
import '@vaadin/vaadin-checkbox/vaadin-checkbox';


export class Grilla extends LitElement {
  static get properties() {
    return {
      userId: { type: String},
      flow: { type: String},
      data: { type: String}
    };
  }

  static get styles() {
    return css`
    mwc-button {
      --mdc-theme-primary: #454545;
    }
    `;
  }

  constructor(){
    super();
    this.flow='cuentas';
    this.data='';
  }

  render() {
    return html`
    <input id="serteoFlowMov" type="hidden" @click=${() => this.seteoMenu('movimientos')}>
    <input id="serteoFlowAltCue" type="hidden" @click=${() => this.informarEvento('flow','altacuentas')}>
    <input id="serteoFlowAltMov" type="hidden" @click=${() => this.informarEvento('flow','altamovimientos')}>
    ${this.flow==='cuentas'?
    html `<h1>Cuentas</h1>
    `
    : html `<h1>Movimientos</h1>`
    }    
      <vaadin-grid >
        <vaadin-grid-column></vaadin-grid-column>
        <vaadin-grid-column></vaadin-grid-column>
        <vaadin-grid-column></vaadin-grid-column>
        <vaadin-grid-column></vaadin-grid-column>
        <vaadin-grid-column></vaadin-grid-column>
        <vaadin-grid-column></vaadin-grid-column>
        <vaadin-grid-column></vaadin-grid-column>
      </vaadin-grid>
      ${this.flow!='cuentas'?
      html `<br><mwc-button raised="" label="Volver" icon="undo" trailingicon="" @click="${() =>this.seteoMenu('cuentas')}"></mwc-button><br>`:  ``
    }
    `
  }

  informarEvento(evento,accion) {
    const grid = this.shadowRoot.querySelector('vaadin-grid');
    let numCuenta = grid.selectItem
    let event = new CustomEvent(evento, {
        detail: {accion:accion,
                 numCuenta:numCuenta}
    });
    this.dispatchEvent(event);
  }

  seteoMenu(opcion) {
    this.flow = opcion;
    this.armarGrilla()
  }

  firstUpdated(){
    this.armarGrilla()
  }

  async armarGrilla(){
    const flow = this.flow
    const serteoFlowMov = this.shadowRoot.getElementById('serteoFlowMov');
    const serteoFlowAltMov = this.shadowRoot.getElementById('serteoFlowAltMov');
    const grid = this.shadowRoot.querySelector('vaadin-grid');
    let url;

    if (flow==='cuentas') {
        url = `http://localhost:3002/api/cuentas/${this.userId}`
    } else {
        url = `http://localhost:3002/api/movimientos/${grid.selectItem}`
    }

  await fetch(url, {method: "GET"}) 
    .then(response => response.json())
    .then(data => {
      if (data.respuesta === 200) {
        this.data = data;
      }  
    })
    grid.heightByRows = true;
    grid.rowsDraggable = true;

    const columns = grid.querySelectorAll('vaadin-grid-column');
    for (let i = 0; i < columns.length; i++) {
      columns[i].autoWidth = true;
      columns[i].renderer = function(root) {root.textContent = ''}
    }

    if (flow==='cuentas' && (this.data.cuenta)) {
      grid.items = this.data.cuenta;
      columns[0].headerRenderer = function(root) {root.textContent = 'Numero Cuenta';}
      columns[1].headerRenderer = function(root) {root.textContent = 'CC/CA';};
      columns[2].headerRenderer = function(root) {root.textContent = 'Sucursal';};
      columns[3].headerRenderer = function(root) {root.textContent = 'Moneda';};
      columns[4].headerRenderer = function(root) {root.textContent = 'Saldo';};
      columns[5].headerRenderer = function(root) {root.textContent = 'Movimientos';};
      columns[6].headerRenderer = function(root) {root.textContent = '';};
      columns[0].renderer = function(root, column, rowData) {root.textContent = rowData.item.numCuenta;} 
      columns[1].renderer = function(root, column, rowData) {root.textContent = rowData.item.tipoCuenta;} 
      columns[2].renderer = function(root, column, rowData) {root.textContent = rowData.item.sucuCuenta;} 
      columns[3].renderer = function(root, column, rowData) {root.textContent = rowData.item.monedaCuenta;} 
      columns[4].renderer = function(root, column, rowData) {root.textContent = rowData.item.saldoCuenta;} 
      columns[5].renderer = function(root, column, rowData) {
        root.innerHTML = '';
        const alertBtn = window.document.createElement('button');
        alertBtn.textContent = 'Movimientos';
        alertBtn.addEventListener('click', function(event) {
          grid.selectItem=rowData.item.numCuenta
          grid.render();
          serteoFlowMov.click()
        });
        const textNode = window.document.createTextNode(' ');
        root.appendChild(alertBtn);
        root.appendChild(textNode);
      };
      columns[6].renderer = function(root, column, rowData) {
        root.innerHTML = '';
        const alertBtn = window.document.createElement('button');
        alertBtn.textContent = 'Operar';
        alertBtn.addEventListener('click', function(event) {
          grid.selectItem=rowData.item.numCuenta
          grid.render();
          serteoFlowAltMov.click()
        });
        const textNode = window.document.createTextNode(' ');
  
        root.appendChild(alertBtn);
        root.appendChild(textNode);
      };      
    } else {
      if (flow==='movimientos' ) {
        grid.items = this.data.movimientos
        columns[0].headerRenderer = function(root) {root.textContent = 'Numero Movimiento';}
        columns[1].headerRenderer = function(root) {root.textContent = 'Tipo Movimiento';}
        columns[2].headerRenderer = function(root) {root.textContent = 'Importe Movimiento';}
        columns[3].headerRenderer = function(root) {root.textContent = 'Destino Movimiento';}
        columns[4].headerRenderer = function(root) {root.textContent = 'Descripción';}
        columns[5].headerRenderer = function(root) {root.textContent = 'Saldo Cuenta';}
        columns[6].headerRenderer = function(root) {root.textContent = 'Fecha';}
        columns[0].renderer = function(root, column, rowData) {root.textContent = (rowData.item.numMov)?rowData.item.numMov:'';} 
        columns[1].renderer = function(root, column, rowData) {root.textContent = (rowData.item.tipMov)?rowData.item.tipMov:'';} 
        columns[2].renderer = function(root, column, rowData) {root.textContent = (rowData.item.impMov)?rowData.item.impMov:'';} 
        columns[3].renderer = function(root, column, rowData) {root.textContent = (rowData.item.destinoMov)?rowData.item.destinoMov:'';} 
        columns[4].renderer = function(root, column, rowData) {root.textContent = (rowData.item.descMov)?rowData.item.descMov:'';} 
        columns[5].renderer = function(root, column, rowData) {root.textContent = (rowData.item.saldoCueMov)?rowData.item.saldoCueMov:'';} 
        columns[6].renderer = function(root, column, rowData) {root.textContent = (rowData.item.saldoCueMov)?rowData.item.fechaAltaMov.substring(0,10):'';} 

      }
    }      
    grid.recalculateColumnWidths()
  }
  
}

customElements.define('practitioner-grilla', Grilla);

