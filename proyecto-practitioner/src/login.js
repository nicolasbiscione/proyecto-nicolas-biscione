import { LitElement, html, css } from 'lit-element';
import "@material/mwc-icon-button";
import "@material/mwc-icon";
import "@material/mwc-button";
import "@material/mwc-textfield";


export class Login extends LitElement {
  static get properties() {
    return {
      title: { type: String },
      page: { type: String },
      activeTab: { type: String },
      tabs: { type: Array },
      smallScreen: { type: Boolean }
    };
  }


  
  static get styles() {
    return css`
      .cuadro-login {
        width: 320px;
        height: 550px;
        background: #005CE5;
        box-shadow: 0 0 30px 0 rgba(255,255,255 ,.4);
        top: 50%;
        left: 50%;
        color: #fff;
        border-radius: 5%;
        position: absolute;
        transform: translate(-50%,-50%);
        box-sizing: border-box;
        padding: 30px 30px;
        display: block;
        text-align: center;
      }
      
      .cuadro-login .img-login {
        width: 100px;
        height: 100px;
        margin-left: auto;
        margin-right: auto;
        border-radius: 50%;
        position: block;
        top: -50px;
      }

      .cuadro-login .h1 {
        margin: 0px;
        padding: 0px;
        font-weight: bold;
        display: block;
      }

      .label-click {
        cursor:pointer;
      }

      mwc-textfield {
        width: 100%;
        margin: 0px;
        margin-bottom: 20px;
        --mdc-theme-primary: white;
        --mdc-shape-small:28px;
        --mdc-text-field-outlined-idle-border-color: rgba(0, 0, 0, 1.1);
        --mdc-text-field-outlined-hover-border-color: rgba(255, 255, 255, 2);
      }

      mwc-button {
        --mdc-theme-primary: #454545;
      }
    `;
  }

  constructor(){
    super();
  }

  render() {
    return html`
      <div class="cuadro-login">
          <img class="img-login" src="img/avatar_login.jpg" alt="avatar">
          <h1>Ingrese</h1>
            <mwc-textfield id="usuario" class="left right";
              required validationMessage="Campo obligatorio" label="Usuario" 
              iconTrailing="perm_identity" outlined>
            </mwc-textfield>
            <mwc-textfield id="password" class="left right";
              required validationMessage="Es obligatorio" label="Password" 
              iconTrailing="vpn_key" type="password" outlined>
            </mwc-textfield>
            <mwc-button raised="" label="Ingresar" icon="undo" trailingicon="" @click="${() => this.realizarlogin()}"></mwc-button>
            <h2 class="label-click" id="Registrar" @click="${() =>this.formularioRegistro()}">Nuevo usuario? Crea tu cuenta</h2>
     </div>
    `;
  }


  informarEvento(evento,flow,data) {
    let mensaje = (data.mensaje)?data.mensaje:''
    let usuario = (data.usuario)?data.usuario:''
    let respuesta = (data.respuesta)?data.respuesta:''
    let event = new CustomEvent(evento, {
        detail: {flow:flow,
                 mensaje:mensaje,
                 respuesta:respuesta,
                 usuario:usuario}
    });
    this.dispatchEvent(event);
  }


  clear(){
    this.clearInputs(this, ["usuario", "password"]);
  }

  formularioRegistro(){
    this.informarEvento('login','registro','');
  }

  async realizarlogin(){
      let usuario = this.getField('usuario').value;
      let password = this.getField('password').value;
      let data = {username: usuario ,password:password};
    await fetch("http://34.221.155.145:3002/api/login", {
        method: "POST",
        mode: "cors",
        headers:{
        "Content-Type": "application/json",
        "Accept": "application/json"},
        body: JSON.stringify(data)
      })
      .then(response => response.json())
      .then(data => {
         if (data.respuesta === 200) {
           localStorage.setItem('token-practitioner',data.token)
         }  
         this.informarEvento('login','login',data);
      })
  }
 
  clearInputs(component, arraysId){
    for (let i = 0; i < arraysId.length; i++){
        this.getField(arraysId[i]).value = "";
    }
  }
  getField(fieldName){
    return this.shadowRoot.getElementById(fieldName);
  }      
}

customElements.define('practitioner-login', Login);
