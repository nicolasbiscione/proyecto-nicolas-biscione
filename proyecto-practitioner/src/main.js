import { LitElement, html, css } from 'lit-element';
import "@material/mwc-icon-button";
import "@material/mwc-icon";
import "@material/mwc-button";
import "@material/mwc-textfield";
import '@vaadin/vaadin-app-layout/vaadin-app-layout.js';
import '@vaadin/vaadin-app-layout/vaadin-drawer-toggle.js';
import '@vaadin/vaadin-tabs/vaadin-tabs.js';
import './cuentas'
import './usuarios'
import './altaturnos'
import './mantTurnos'
import './reporteturnos'

export class Login extends LitElement {
  static get properties() {
    return {
      userId: { type: String},
      userName: { type: String},
      nombre: { type: String},
      apellido: { type: String},
      admin: { type: Boolean},
      email: {type: String},
      flow: {type: String},
      valDolOfiCmp: {type: String},
      valDolOfiVta: {type: String},
      valDolBlueCmp: {type: String},
      valDolBlueVta: {type: String},
      valDolTurCmp: {type: String},
      valDolTurVta: {type: String}
    };
  }

  static get styles() {
    return css`
      .contenido {
        width: 100%;      
        height: 100%;
        top: 50%;
        left: 50%;
        color: #fff;
        box-sizing: border-box;
        padding: 30px 30px;
        display: block;
        text-align: center;
      }

      vaadin-tabs  {
        background-color: #bcc0ba;
        padding: 8px;
        width: auto;      
        height: 96%;
      }

      .cuadro {
        width: 50%;      
        height: 100%;
        background: #00A0D7;
        top: 50%;
        left: 50%;
        color: #fff;
        position: absolute;
        transform: translate(-50%,-50%);
        box-sizing: border-box;
        padding: 30px 30px;
        display: block;
        text-align: center;
      }

      .banner-dol {
        width: 95%;      
        margin: auto;
        overflow: hidden;
      }

      .banner-dol ul{
        display: flex;      
        padding: 0;
        width: 300%;      
        animation: cambio 10s infinite;
        animation-direction: alternate;
      }

      .banner-dol li{
        width: 100%;      
        list-style: none;
      }
  
      .banner-dol h2{
        width: 100%; 
        color: white;     
      }

      @keyframes cambio{
          0% {margin-left:0;}
          22% {margin-left:0;}

          27% {margin-left: -100%;}
          73% {margin-left: -100%;}

          78% {margin-left: -200%;}
          100% {margin-left: -200%;}

        }
    `;
  }

  constructor(){
    super();
    this.admin=false;
  }

  render() {
    return html`
    <div>
      <vaadin-app-layout primary-section="navbar|drawer">
      <vaadin-drawer-toggle slot="navbar [touch-optimized]"></vaadin-drawer-toggle>
      <h3 slot="navbar [touch-optimized]">${this.nombre} ${this.apellido} / ${this.email} </h3>
      <div class="banner-dol">
        <ul>
          <li><h2>Dolar Oficial - Compra: $${this.valDolOfiCmp} / Venta: $${this.valDolOfiVta}</h2></li>
          <li><h2>Dolar Blue - Compra: $${this.valDolBlueCmp} / Venta: $${this.valDolBlueVta}</h2></li>
          <li><h2>Dolar Turista - Compra: ${this.valDolTurCmp} / Venta: $${this.valDolTurVta}</h2></li>
        </ul>  
      </div>
      <vaadin-tabs slot="drawer" orientation="vertical" theme="minimal"  >
      ${(this.admin===false)?
        html `<vaadin-tab @click=${() => this.seteoMenu('cuentas')}>Cuentas</vaadin-tab>
        <vaadin-tab @click=${() => this.seteoMenu('turnos')}>Solicitar Turno</vaadin-tab>`:
        html `<vaadin-tab @click=${() => this.seteoMenu('usuarios')}>ABM Usuarios</vaadin-tab>
        <vaadin-tab @click=${() => this.seteoMenu('mantenimientoturnos')}>Administración Turnos</vaadin-tab>
        <vaadin-tab @click=${() => this.seteoMenu('reporteturnos')}>Reporte Turnos</vaadin-tab>`
      }  
        <vaadin-tab @click=${() => this.logout('logout')}>Logout</vaadin-tab>
      </vaadin-tabs>
      <div class= "contenido" >
        ${(this.flow==='cuentas')?
        html `<practitioner-cuentas userId=${this.userId} ></practitioner-cuentas>`:``}
        ${(this.flow==='turnos')?
        html `<practitioner-altaturno userId=${this.userId} ></practitioner-altaturno>`:``}
        ${(this.flow==='usuarios')?
        html `<practitioner-usuarios userId=${this.userId} ></practitioner-usuarios>`:``}
        ${(this.flow==='mantenimientoturnos')?
        html `<practitioner-manturnos userId=${this.userId} ></practitioner-manturnos>`:``}
        ${(this.flow==='reporteturnos')?
        html `<practitioner-reporteturnos userId=${this.userId} ></practitioner-reporteturnos>`:``}
      </div>   
      <div id="outlet"></div>
      </vaadin-app-layout>
    </div>  
    `;
  }

  async firstUpdated(){
    let url = `http://localhost:3002/api/usuarios/${this.userId}`

    await fetch(url, {method: "GET"}) 
      .then(response => response.json())
      .then(data => {
        if (data.respuesta === 200) {
          this.email = data.usuario.email;
          this.nombre = data.usuario.nombre;
          this.apellido = data.usuario.apellido;
          this.admin = (data.usuario.admin)?data.usuario.admin:false;
        }  
      })
      
      url = `https://www.dolarsi.com/api/api.php?type=valoresprincipales`

      await fetch(url, {method: "GET"}) 
        .then(response => response.json())
        .then(data => {
          for (let i = 0; i < data.length; i++) {
            if (data[i].casa.agencia === '349'){
              this.valDolOfiCmp = data[i].casa.compra
              this.valDolOfiVta = data[i].casa.venta
            }
            if (data[i].casa.agencia === '310'){
              this.valDolBlueCmp = data[i].casa.compra
              this.valDolBlueVta = data[i].casa.venta
            }
            if (data[i].casa.agencia === '406'){
              this.valDolTurCmp = data[i].casa.compra
              this.valDolTurVta = data[i].casa.venta
            }
          }
        })
      
    if (this.admin===false) {
      this.flow = 'cuentas'
    } else {
      this.flow = 'usuarios'
    }
  }

  seteoMenu(opcion) {
    this.flow = opcion;
  }

  informarEvento(evento,flow,data) {
    let mensaje = (data.mensaje)?data.mensaje:''
    let event = new CustomEvent(evento, {
        detail: {flow:flow,
                mensaje:mensaje}
    });
    this.dispatchEvent(event);
  }

  logout(){
    localStorage.removeItem('token-practitioner')
    this.informarEvento('logout','login','');
  }    
}

customElements.define('practitioner-main', Login);
