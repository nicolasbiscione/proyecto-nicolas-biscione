import { LitElement, html, css } from 'lit-element';
import "@material/mwc-icon-button";
import "@material/mwc-icon";
import "@material/mwc-button";
import "@material/mwc-textfield";
import "@material/mwc-textfield";
import '@vaadin/vaadin-grid/vaadin-grid.js';
import '@vaadin/vaadin-checkbox/vaadin-checkbox';


export class MantTurnos extends LitElement {
  static get properties() {
    return {
      userId: { type: String},
      flow: { type: String},
      sucursal:  { type: String},
      cantTurnosHora:  { type: Number}
    };
  }

  static get styles() {
    return css`
      .contenido {
        width: 0%;      
        height: 100%;
        background: #005CE5;
        box-shadow: 0 0 15px #ddd;
        top: 50%;
        left: 50%;
        color: #fff;
        box-sizing: border-box;
        padding: 30px 30px;
        display: block;
        text-align: center;
        border-radius:10px;
        opacity: 0;
        animation-name: carga;
        animation-duration: 1s; 
        animation-iteration-count: 1;
        animation-fill-mode: forwards;
      }
      
      @keyframes carga{
        from {opacity: 0;}
        to {opacity: 1;}
        from {width: 1%;}
        to {width: 100%;}
      }
      
      .mantturnos-form {
        display: grid;
        grid-template-columns: 1fr;
        padding: 1em;
      } 

      mwc-select ,
      mwc-textfield {
        width: 50%;
        margin: auto;
        padding: .8em;
        --mdc-theme-primary: white;
        --mdc-shape-small:28px;
        --mdc-text-field-outlined-idle-border-color: rgba(0, 0, 0, 1.1);
        --mdc-text-field-outlined-hover-border-color: rgba(255, 255, 255, 2);
        --mdc-select-outlined-idle-border-color: rgba(0, 0, 0, 1.1);
        --mdc-select-outlined-hover-border-color: rgba(255, 255, 255, 2);
      }

      mwc-button {
        --mdc-theme-primary: #454545;
      }
    
    `;
  }

  constructor(){
    super();
    this.flow = 'mantenimiento'
    this.cantTuenosHora = ''
  }

  render() {
    return html `
    <div class="contenido">
      <h1>Parametria de turnos</h1>
      <h2>Cantidad de turnos por hora</h2>
      <div class="mantturnos-form">
        <mwc-select id="sucursal" required  label="Sucursal"
          outlined @click=${ this.validarDatos } @change=${ this.completarInput} >         
          <mwc-list-item value="Caseros">Caseros</mwc-list-item>
          <mwc-list-item value="Caballito">Caballito</mwc-list-item>
          <mwc-list-item value="Olivos">Olivos</mwc-list-item>
        </mwc-select>
        <mwc-textfield id="cantTurnosHora"  required validationMessage="Campo obligatorio" label="Cantidad de turnos" 
          type="number" outlined @input=${ this.validarDatos } >
        </mwc-textfield>
        </mwc-select>
      </div>        
      <mwc-button raised="" id="modificar" label="Modificar" icon="undo" trailingicon="" @click="${this.altaDisponibilidad}" disabled></mwc-button>
    </div>
    `
  }

  async completarInput(){
    if (this.getField('sucursal').checkValidity()) {
      this.sucursal = this.getField('sucursal').value
      let url = `http://localhost:3002/api/disponibilidad/${this.sucursal}`
      await fetch(url, {method: "GET"}) 
        .then(response => response.json())
        .then(data => {
          if (data.respuesta === 200) {
            this.getField('cantTurnosHora').value = data.disponibilidad.cantTuenosHora;
          }  
        })
    }    
  } 

  async validarDatos(){

    if (this.getField('sucursal').checkValidity() &&
        this.getField('cantTurnosHora').checkValidity()) {
          this.getField('modificar').disabled = false
    } else {
          this.getField('modificar').disabled = false
    }
  }
  

  informarEvento(evento,accion) {
    const grid = this.shadowRoot.querySelector('vaadin-grid');
    let numCuenta = grid.selectItem
    let event = new CustomEvent(evento, {
        detail: {accion:accion,
                 numCuenta:numCuenta}
    });
    this.dispatchEvent(event);
  }

  informarMensajes(evento,flow,data) {
    let mensaje = (data.mensaje)?data.mensaje:''
    let event = new CustomEvent(evento, {
        bubbles: true,
        composed: true,
        detail: {flow:flow,
                mensaje:mensaje}
    });
    this.dispatchEvent(event);
  }

  async  altaDisponibilidad(){
    this.cantTurnosHora= this.getField('cantTurnosHora').value;
    this.sucursal = this.getField('sucursal').value;
    let data = {cantTuenosHora:this.cantTurnosHora, sucursal: this.sucursal };
    await fetch("http://localhost:3002/api/disponibilidad", {
          method: "POST",
          mode: "cors",
          headers:{
          "Content-Type": "application/json",
          "Accept": "application/json"
          },
          body: JSON.stringify(data)
          })
          .then(response => response.json())
          .then(data => {
              if (data.respuesta === 200) {
                this.informarMensajes('mensajes','turnos',data);
              }
          })

  }

  getField( fieldName){
    return this.shadowRoot.getElementById(fieldName);
  }  
}

customElements.define('practitioner-manturnos', MantTurnos);

