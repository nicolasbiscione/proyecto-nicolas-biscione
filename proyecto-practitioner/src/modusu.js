import { LitElement, html, css } from 'lit-element';
import "@material/mwc-icon-button";
import "@material/mwc-icon";
import "@material/mwc-button";
import "@material/mwc-textfield";
import '@material/mwc-select';
import '@material/mwc-list/mwc-list-item';
import '@material/mwc-checkbox';


export class ModUsu extends LitElement {
  static get properties() {
    return {
      userId: String,
      nombre: String,
      apellido: String,
      email: String,
      admin: Boolean
    };
  }

  static get styles() {
    return css`
      .modusu-form {
        display: grid;
        grid-template-columns: 1fr 1fr;
      }

      .modusu-form .block {
        grid-column: 1/3;
      }      

      mwc-select,
      mwc-textfield {
        width: 90%;
        margin: auto;
        padding: .8em;
        --mdc-theme-primary: white;
        --mdc-shape-small:28px;
        --mdc-text-field-outlined-idle-border-color: rgba(0, 0, 0, 1.1);
        --mdc-text-field-outlined-hover-border-color: rgba(255, 255, 255, 2);
        --mdc-select-outlined-idle-border-color: rgba(0, 0, 0, 1.1);
        --mdc-select-outlined-hover-border-color: rgba(255, 255, 255, 2);
        }

      mwc-button {
        --mdc-theme-primary: #454545;
      }
     
    `;
  }

  constructor(){
    super();
  }




  render() {
    return html`
        <div class="modusu-form">
          <mwc-textfield id="nombre" 
              required validationMessage="Es obligatorio" label="Nombre" value=${this.nombre}
              outlined @click="${ this.validarDatos }">
          </mwc-textfield>
          <mwc-textfield id="apellido" 
              required validationMessage="Es obligatorio" label="Apellido"  value=${this.apellido}
              outlined @click="${ this.validarDatos }">
          </mwc-textfield>
          <mwc-textfield id="email"  class="block"
              required validationMessage="Es obligatorio" label="Correo electronico"  value=${this.email}
              type="email" outlined @input="${ this.validarDatos }" >
          </mwc-textfield>
          <label for="admin" class="block">Administrador?</label>
          <mwc-checkbox id="admin" class="block""; outlined @input="${ this.validarDatos }" >
          </mwc-checkbox>
        </div>        
        <mwc-button raised="" id="modificar" label="Modificar" icon="undo" trailingicon="" @click="${this.modificarUsuario}" disabled></mwc-button>
        <mwc-button raised="" id="cancelar" label="Cancelar" icon="undo"  @click=${() => this.informarEvento('flow','usuarios')}" ></mwc-button>
    `;
  }

  async firstUpdated(){
    let url = `http://localhost:3002/api/usuarios/${this.userId}`

    await fetch(url, {method: "GET"}) 
      .then(response => response.json())
      .then(data => {
        if (data.respuesta === 200) {
          this.nombre = data.usuario.nombre;
          this.apellido = data.usuario.apellido;
          this.email = data.usuario.email;
          this.admin = data.usuario.admin;
        }  
      })
      if (this.admin){
        this.getField('admin').checked = true
      } else {
        this.getField('admin').checked = false
      }
      this.validarDatos()
  }


  informarEvento(evento,accion) {
    let event = new CustomEvent(evento, {
        detail: {accion:accion}
    });
    this.dispatchEvent(event);
  }


  validarDatos(){
    if (this.getField('nombre').checkValidity() &&
        this.getField('apellido').checkValidity() &&
        this.getField('email').checkValidity()) {
          this.getField('modificar').disabled = false
    } else {
          this.getField('modificar').disabled = true
    }
  }
  
  informarMensajes(evento,flow,data) {
    let mensaje = (data.mensaje)?data.mensaje:''
    let event = new CustomEvent(evento, {
        bubbles: true,
        composed: true,
        detail: {flow:flow,
                mensaje:mensaje}
    });
    this.dispatchEvent(event);
  }  
  
async  modificarUsuario(){
    this.nombre= this.getField('nombre').value;
    this.apellido= this.getField('apellido').value;
    this.email= this.getField('email').value;
    this.admin= this.getField('admin').checked;
    let data = { nombre: this.nombre ,apellido:this.apellido,email:this.email,admin:this.admin};
    await fetch(`http://localhost:3002/api/usuarios/${this.userId}`, {
      method: "PUT",
      mode: "cors",
      headers:{
      "Content-Type": "application/json",
      "Accept": "application/json"
      },
      body: JSON.stringify(data)
      })
      .then(response => response.json())
      .then(data => {
          if (data.respuesta === 200) {
            this.informarEvento('flow','usuarios');
            this.informarMensajes('mensajes','modusu',data);
          }
      })
  }
 
  getField( fieldName){
    return this.shadowRoot.getElementById(fieldName);
  }      
  
}

customElements.define('practitioner-modusu', ModUsu);
