import { LitElement, html, css } from 'lit-element';
import "@material/mwc-icon-button";
import "@material/mwc-icon";
import "@material/mwc-button";
import "@material/mwc-textfield";


export class Registro extends LitElement {
  static get properties() {
    return {
      validacionOK: Boolean,
      usuario: String,
      password: String,
      confPassword: String,
      nombre: String,
      apellido: String,
      email: String,
    };
  }

  static get styles() {
    return css`
    .cuadro {
      width: 720px;
      height: 650px;
      background: #005CE5;
      box-shadow: 0 0 30px 0 rgba(255,255,255 ,.4);
      top: 50%;
      left: 50%;
      color: #fff;
      border-radius: 5%;
      position: absolute;
      transform: translate(-50%,-50%);
      box-sizing: border-box;
      padding: 30px 30px;
      display: block;
      text-align: center;
    }
    
    .registro-form {
      display: grid;
      grid-template-columns: 1fr 1fr;
      padding: 1em;
    }

    .registro-form .block {
      grid-column: 1/3;
    }  

    mwc-button {
      --mdc-theme-primary: #454545;
    }

    mwc-textfield {
      width: 100%;
      margin: 10px;
      margin-bottom: 10px;
      --mdc-theme-primary: white;
      --mdc-shape-small:28px;
      --mdc-text-field-outlined-idle-border-color: rgba(0, 0, 0, 1.1);
      --mdc-text-field-outlined-hover-border-color: rgba(255, 255, 255, 2);
      --mdc-select-outlined-idle-border-color: rgba(0, 0, 0, 1.1);
      --mdc-select-outlined-hover-border-color: rgba(255, 255, 255, 2);
    }

    `;
  }

  constructor(){
    super();
    this.usuario='';
    this.password='';
    this.confPassword='';
    this.nombre='';
    this.apellido='';
    this.email='';
  }

  render() {
    return html`
    <div class="cuadro">
      <br/>
      <h1>Registro</h1>
      <div class="registro-form">
          <mwc-textfield id="usuario" class="block";
            required validationMessage="Campo obligatorio" label="Usuario" 
            iconTrailing="perm_identity" outlined @click="${ this.validarDatos }">
          </mwc-textfield>
          <mwc-textfield id="password" required validationMessage="Es obligatorio" label="Password" 
            iconTrailing="vpn_key" type="password" outlined @click="${ this.validarDatos }">
          </mwc-textfield>
          <mwc-textfield id="confPassword" required validationMessage="Es obligatorio" label="Confirmar password" 
              iconTrailing="vpn_key" type="password" outlined @click="${ this.validarDatos }">
          </mwc-textfield> 
          <mwc-textfield id="nombre" required validationMessage="Es obligatorio" label="Nombre"
                outlined @click="${ this.validarDatos }">
          </mwc-textfield>
          <mwc-textfield id="apellido" required validationMessage="Es obligatorio" label="Apellido" 
              outlined @click="${ this.validarDatos }">
          </mwc-textfield>
          <mwc-textfield id="email" class="block"; 
              required validationMessage="Es obligatorio" label="Correo electronico"  
              type="email" outlined @input="${ this.validarDatos }" >
          </mwc-textfield>
      </div>
      <mwc-button raised="" id="registrar" label="Registrar" icon="undo" trailingicon="" @click="${this.realizarRegistro}" disabled></mwc-button>
      <mwc-button raised="" label="Cancelar" icon="undo" trailingicon="" @click="${() =>this.cancelarRegistro()}"></mwc-button>
    </div>

    `;
  }

  informarEvento(evento,flow,data) {
    let mensaje = (data.mensaje)?data.mensaje:''
    let event = new CustomEvent(evento, {
        detail: {flow:flow,
                mensaje:mensaje}
    });
    this.dispatchEvent(event);
  }

  clear(){
    this.clearInputs(this, ["usuario", "password"]);
  }

  cancelarRegistro(){
    this.informarEvento('registro','login','');
  }

  validarDatos(){
    if (this.getField('usuario').checkValidity() &&
        this.getField('password').checkValidity() &&
        this.getField('nombre').checkValidity() &&
        this.getField('apellido').checkValidity() &&
        this.getField('email').checkValidity() &&
        this.getField('confPassword').checkValidity()){
          this.getField('registrar').disabled = false
    } else {
          this.getField('registrar').disabled = true
    }
  }
  
  realizarRegistro(){
      this.usuario= this.getField('usuario').value;
      this.password= this.getField('password').value;
      this.confPassword= this.getField('confPassword').value;
      this.nombre= this.getField('nombre').value;
      this.apellido= this.getField('apellido').value;
      this.email= this.getField('email').value;
      let data = {username: this.usuario ,password:this.password,nombre:this.nombre,apellido:this.apellido,email:this.email};
      if (this.password != this.confPassword){
        let error = {mensaje:'La contraseña ingresada no es valida'}
        this.informarEvento('registro','registro',error);
      }  else {
        fetch("http://localhost:3002/api/registro", {
        method: "POST",
        mode: "cors",
        headers:{
        "Content-Type": "application/json",
        "Accept": "application/json"
        },
        body: JSON.stringify(data)
        })
        .then(response => response.json())
        .then(data => {
            if (data.respuesta === 200) {
              this.informarEvento('registro','login',data);
            }  else {
              this.informarEvento('registro','registro',data);
            }
        })
      }
   }
   
  clearInputs(component, arraysId){
    for (let i = 0; i < arraysId.length; i++){
        this.getField(component, arraysId[i]).value = "";
    }
  }
  getField( fieldName){
    return this.shadowRoot.getElementById(fieldName);
  }      
  
}
customElements.define('practitioner-registro', Registro);
