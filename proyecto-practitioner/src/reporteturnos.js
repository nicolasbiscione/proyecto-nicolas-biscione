import { LitElement, html, css } from 'lit-element';
import '@vaadin/vaadin-grid/vaadin-grid.js';
import '@vaadin/vaadin-date-picker/vaadin-date-picker';
import "@material/mwc-button";

export class ReporteTurnos extends LitElement {
  static get properties() {
    return {
      userId: { type: String},
      flow: { type: String},
      data: { type: String}
    };
  }

  static get styles() {
    return css`
      .contenido {
        width: 0%;      
        height: 100%;
        background: #005CE5;
        box-shadow: 0 0 15px #ddd;
        top: 50%;
        left: 50%;
        color: #fff;
        box-sizing: border-box;
        padding: 30px 30px;
        display: block;
        text-align: center;
        border-radius:10px;
        opacity: 0;
        animation-name: carga;
        animation-duration: 1s; 
        animation-iteration-count: 1;
        animation-fill-mode: forwards;
      }
      
      @keyframes carga{
        from {opacity: 0;}
        to {opacity: 1;}
        from {width: 1%;}
        to {width: 100%;}
      }

      .reporte-form {
        display: grid;
        grid-template-columns: 1fr;
        padding: 1em;
      } 

      vaadin-date-picker {
        width: 30%;
        margin: auto;
        padding: .8em;
        color: #fff;
        --lumo-header-text-color: ##000000;;
        --lumo-body-text-color: #ffffff;
        --lumo-secondary-text-color: #000000;
        --lumo-tertiary-text-color:#ffffff;
        --lumo-disabled-text-color:#000000;
        --lumo-primary-color-10pct: #000000;
        --lumo-primary-color-50pct: #000000;
        --lumo-primary-text-color: #ffffff;
        -webkit-backdrop-filter: blur(60px);
      }
      
      mwc-button {
        --mdc-theme-primary: #454545;
        padding: 1em;
      }
    `;
  }

  constructor(){
    super();
    this.data='';
  }

  render() {
    return html`
      <div class="contenido">
        <h1>Reporte de turnos por fecha</h1>
        <div class="reporte-form">
          <vaadin-date-picker id="fecha" theme="align-center" label="Seleccione la fecha">
          </vaadin-date-picker>
        </div>  
        <mwc-button raised="" id="alta" label="Consultar" icon="undo" trailingicon="" @click="${this.armarGrilla}"></mwc-button>
        <vaadin-grid >
          <vaadin-grid-column></vaadin-grid-column>
          <vaadin-grid-column></vaadin-grid-column>
          <vaadin-grid-column></vaadin-grid-column>
          <vaadin-grid-column></vaadin-grid-column>
          <vaadin-grid-column></vaadin-grid-column>
        </vaadin-grid>
      </div>  
    `
  }

  informarEvento(evento,accion) {
    const grid = this.shadowRoot.querySelector('vaadin-grid');
    let numCuenta = grid.selectItem
    let event = new CustomEvent(evento, {
        detail: {accion:accion,
                 numCuenta:numCuenta}
    });
    this.dispatchEvent(event);
  }

  firstUpdated(){
    this.armarGrilla()
    var datepicker = this.shadowRoot.querySelector('vaadin-date-picker#fecha');
      datepicker.set('i18n.weekdaysShort', ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab']);
      datepicker.set('i18n.monthNames', ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
                                        'Junio', 'Julio', 'Agosto', 'Septiembre',
                                        'Octubre', 'Noviembre', 'Diciembre']);
      datepicker.set('i18n.formatDate', function(dateObject) {
         return [dateObject.day,dateObject.month+1, dateObject.year].join('/');
      });
      datepicker.set('i18n.parseDate', function(text) {
        var parts = text.split('/');
        if (parts.length === 3) {
          return {
            day: parts[0],
            month: parts[1]-1,
            year: parts[2]
          };
        }  
      });            
  }

  informarMensajes(evento,flow,data) {
    let mensaje = (data.mensaje)?data.mensaje:''
    let event = new CustomEvent(evento, {
        bubbles: true,
        composed: true,
        detail: {flow:flow,
                mensaje:mensaje}
    });
    this.dispatchEvent(event);
  }

  async armarGrilla(){
    const grid = this.shadowRoot.querySelector('vaadin-grid');
    let fecha = this.getField('fecha').value;
    if (fecha===''){
      fecha = '1900-01-01'
    }
    let url = `http://localhost:3002/api//turnosFecha/${fecha}`
 
  await fetch(url, {method: "GET"}) 
    .then(response => response.json())
    .then(data => {
      if (data.respuesta === 200) {
        this.data = data;
        this.informarMensajes('mensajes','repturnos',data);
      }  
    })
    grid.heightByRows = true;
    grid.rowsDraggable = true;

    const columns = grid.querySelectorAll('vaadin-grid-column');
    for (let i = 0; i < columns.length; i++) {
      columns[i].autoWidth = true;
      columns[i].renderer = function(root) {root.textContent = ''}
    }

    grid.items = this.data.turnos;
    columns[0].headerRenderer = function(root) {root.textContent = 'Cliente';}
    columns[1].headerRenderer = function(root) {root.textContent = 'Email';}
    columns[2].headerRenderer = function(root) {root.textContent = 'Fecha';};
    columns[3].headerRenderer = function(root) {root.textContent = 'Hora';};
    columns[4].headerRenderer = function(root) {root.textContent = 'Sucursal';};
    columns[0].renderer = function(root, column, rowData) {root.textContent = rowData.item.nombre +  ' ' +rowData.item.apellido;} 
    columns[1].renderer = function(root, column, rowData) {root.textContent = rowData.item.email;} 
    columns[2].renderer = function(root, column, rowData) {root.textContent = rowData.item.fecha;} 
    columns[3].renderer = function(root, column, rowData) {root.textContent = rowData.item.hora;} 
    columns[4].renderer = function(root, column, rowData) {root.textContent = rowData.item.sucursal;} 
    grid.recalculateColumnWidths()
  }
  
  getField( fieldName){
    return this.shadowRoot.getElementById(fieldName);
  }   
}

customElements.define('practitioner-reporteturnos', ReporteTurnos);

