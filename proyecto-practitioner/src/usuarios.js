import { LitElement, html, css } from 'lit-element';
import "@material/mwc-icon-button";
import "@material/mwc-icon";
import "@material/mwc-button";
import "@material/mwc-textfield";
import "@material/mwc-textfield";
import '@vaadin/vaadin-grid/vaadin-grid.js';
import '@vaadin/vaadin-checkbox/vaadin-checkbox';
import './modusu.js'


export class Usuarios extends LitElement {
  static get properties() {
    return {
      userId: { type: String},
      userIdSelec: { type: String},
      flow: { type: String},
      data: { type: String}
    };
  }

  static get styles() {
    return css`
    .contenido {
      width: 0%;      
      height: 100%;
      background: #005CE5;
      box-shadow: 0 0 15px #ddd;
      top: 50%;
      left: 50%;
      color: #fff;
      box-sizing: border-box;
      padding: 30px 30px;
      display: block;
      text-align: center;
      border-radius:10px;
      opacity: 0;
      animation-name: carga;
      animation-duration: 1s; 
      animation-iteration-count: 1;
      animation-fill-mode: forwards;
    }
    
    @keyframes carga{
      from {opacity: 0;}
      to {opacity: 1;}
      from {width: 1%;}
      to {width: 100%;}
    }
    `;
  }

  constructor(){
    super();
    this.flow='usuarios';
    this.data='';
    this.userIdSelec = '';
  }

  render() {
    return html`
    <div class="contenido">
      <input id="serteoFlowMod" type="hidden" @click=${() => this.seteoMenu('modificar')}>
      <input id="serteoFlowBaja" type="hidden" @click=${() => this.borrarUsuario()}>
      <h1>Usuarios</h1>
      <br>
      <vaadin-grid theme="no-border">
          <vaadin-grid-column></vaadin-grid-column>
          <vaadin-grid-column></vaadin-grid-column>
          <vaadin-grid-column></vaadin-grid-column>
          <vaadin-grid-column></vaadin-grid-column>
          <vaadin-grid-column></vaadin-grid-column>
          <vaadin-grid-column></vaadin-grid-column>
          <vaadin-grid-column></vaadin-grid-column>
        </vaadin-grid>
        ${(this.flow==='modificar')?
        html `<br><practitioner-modusu userId=${this.userIdSelec} @flow=${this.flowGrilla}></practitioner-modusu>`:  ``
        }
    </div>    
    `
  }

  flowGrilla(e) {
    this.seteoMenu(e.detail.accion)
  }

  informarEvento(evento,accion) {
    const grid = this.shadowRoot.querySelector('vaadin-grid');
    let numCuenta = grid.selectItem
    let event = new CustomEvent(evento, {
        detail: {accion:accion,
                 numCuenta:numCuenta}
    });
    this.dispatchEvent(event);
  }

  seteoMenu(opcion) {
    this.flow = opcion;
    const grid = this.shadowRoot.querySelector('vaadin-grid');
    this.userIdSelec = grid.selectItem
    this.armarGrilla()
  }

  firstUpdated(){
    this.armarGrilla()
  }

  informarMensajes(evento,flow,data) {
    let mensaje = (data.mensaje)?data.mensaje:''
    let event = new CustomEvent(evento, {
        bubbles: true,
        composed: true,
        detail: {flow:flow,
                mensaje:mensaje}
    });
    this.dispatchEvent(event);
  }

  async borrarUsuario(){

    let borrar = confirm('Confirma borrar este usuario?')

    if (borrar) {
      const grid = this.shadowRoot.querySelector('vaadin-grid');
      this.userIdSelec = grid.selectItem
      let url = `http://localhost:3002/api/usuarios/${this.userIdSelec}`


    await fetch(url, {method: "DELETE"}) 
      .then(response => response.json())
      .then(data => {
        if (data.respuesta === 200) {
          this.data = data;
          this.informarMensajes('mensajes','borrusuario',data);
          this.armarGrilla();
        }  
      })
    }
  }  

  async armarGrilla(){

    const flow = this.flow
    const serteoFlowMod = this.shadowRoot.getElementById('serteoFlowMod');
    const serteoFlowBaja = this.shadowRoot.getElementById('serteoFlowBaja');
    const grid = this.shadowRoot.querySelector('vaadin-grid');
    let url = `http://localhost:3002/api/usuarios`


  await fetch(url, {method: "GET"}) 
    .then(response => response.json())
    .then(data => {
      console.log(data)
      if (data.respuesta === 200) {
        this.data = data;
      }  
    })
    grid.heightByRows = true;
    grid.rowsDraggable = true;

    const columns = grid.querySelectorAll('vaadin-grid-column');
    for (let i = 0; i < columns.length; i++) {
      columns[i].autoWidth = true;
      columns[i].renderer = function(root) {root.textContent = ''}
    }


    grid.items = this.data.usuario;
    columns[0].headerRenderer = function(root) {root.textContent = 'Usuario';}
    columns[1].headerRenderer = function(root) {root.textContent = 'Nombre';};
    columns[2].headerRenderer = function(root) {root.textContent = 'Apellido';};
    columns[3].headerRenderer = function(root) {root.textContent = 'Email';};
    columns[4].headerRenderer = function(root) {root.textContent = 'Administrador?';};
    //columns[5].headerRenderer = function(root) {root.textContent = 'Movimientos';};
    columns[0].renderer = function(root, column, rowData) {root.textContent = rowData.item.username;} 
    columns[1].renderer = function(root, column, rowData) {root.textContent = rowData.item.nombre;} 
    columns[2].renderer = function(root, column, rowData) {root.textContent = rowData.item.apellido;} 
    columns[3].renderer = function(root, column, rowData) {root.textContent = rowData.item.email;} 
    columns[4].renderer = function(root, column, rowData) {root.textContent = rowData.item.admin;} 
    if (this.flow!='modificar'){
      columns[5].renderer = function(root, column, rowData) {
        root.innerHTML = '';
        const alertBtn = window.document.createElement('button');
        alertBtn.textContent = 'Modificar';
        alertBtn.addEventListener('click', function(event) {
          grid.selectItem=rowData.item._id
          grid.render();
          serteoFlowMod.click()
        });
        const textNode = window.document.createTextNode(' ');

        root.appendChild(alertBtn);
        root.appendChild(textNode);
      };
      columns[6].renderer = function(root, column, rowData) {
        root.innerHTML = '';
        const alertBtn = window.document.createElement('button');
        alertBtn.textContent = 'Borrar';
        alertBtn.addEventListener('click', function(event) {
          grid.selectItem=rowData.item._id
          grid.render();
          serteoFlowBaja.click()
        });
        const textNode = window.document.createTextNode(' ');

        root.appendChild(alertBtn);
        root.appendChild(textNode);
          
      grid.recalculateColumnWidths()
      }
    }  
  }
  
}

customElements.define('practitioner-usuarios', Usuarios);

