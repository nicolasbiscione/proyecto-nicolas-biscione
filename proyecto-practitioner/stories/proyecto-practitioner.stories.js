import { html } from 'lit-html';
import '../src/proyecto-practitioner.js';

export default {
  title: 'proyecto-practitioner',
};

export const App = () =>
  html`
    <proyecto-practitioner></proyecto-practitioner>
  `;
